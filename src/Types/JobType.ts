export type JobType = {
  job: string
  employer: string
  location: string
  from: string
  to?: string
  responsibilities: string[]
}
