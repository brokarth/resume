import { FunctionComponent, ImgHTMLAttributes } from 'react'

type ImageProps = ImgHTMLAttributes<HTMLImageElement> & {
  src: string
  alt: string
}

export const Image: FunctionComponent<ImageProps> = ({ src, alt, ...props }) => (
  <img src={`${process.env.NODE_ENV === 'production' ? '/resume' : ''}${src}`} alt={alt} {...props} />
)
