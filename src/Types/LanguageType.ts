export type LanguageType = {
  title: string
  level: number
}
