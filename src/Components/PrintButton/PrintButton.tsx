import { Image } from '@src/Components/Image'
import styles from './PrintButton.module.scss'

export const PrintButton = () => {
  const handleClick = () => window.print()
  return (
    <button id={styles.printButton} onClick={handleClick}>
      <Image className={styles.printIcon} src="/images/printer.png" alt="Imprimir" />

      <span>Imprimir</span>
    </button>
  )
}
