import { Image } from '@src/Components/Image'
import { LanguageType } from '@src/Types/LanguageType'
import { FunctionComponent } from 'react'
import styles from './Languages.module.scss'

export const Languages: FunctionComponent<{ languages: LanguageType[] }> = ({ languages }) => (
  <main id={styles.languages}>
    <Image src="/images/earth.png" alt="" />
    <h1>Languages</h1>
    <div className={styles.languagesList}>
      {languages.map(({ title, level }) => (
        <section key={title}>
          <h2>
            {title}
            {level === 100 && <span>(native)</span>}
          </h2>
          <progress value={level} max="100">
            {level}%
          </progress>
        </section>
      ))}
    </div>
  </main>
)
