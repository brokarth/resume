import resumeData from '@data/resume.json'
import { EmploymentHistory } from '@src/Components/EmploymentHistory'
import { Languages } from '@src/Components/Languages'
import { PersonalInformation } from '@src/Components/PersonalInformation'
import { PrintButton } from '@src/Components/PrintButton'
import { ProfessionalSummary } from '@src/Components/ProfessionalSummary'
import { Skills } from '@src/Components/Skills'
import type { NextPage } from 'next'
import Head from 'next/head'
import styles from './Home.module.scss'

export const Home: NextPage = () => {
  const { personalInformation, summary, jobHistory, skills, languages } = resumeData
  const { fullName } = personalInformation
  return (
    <>
      <Head>
        <title>{fullName} Resume</title>
        <meta name="description" content={`Resume of ${fullName}`} />
      </Head>

      <main className={styles.resume}>
        <section className={styles.printButton}>
          <PrintButton />
        </section>
        <section className={styles.personalInformation}>
          <PersonalInformation personalInformation={personalInformation} />
        </section>
        <section className={styles.jobExperience}>
          <ProfessionalSummary summary={summary} />
          <EmploymentHistory jobHistory={jobHistory} />
        </section>
        <aside className={styles.skills}>
          <Skills skills={skills} />
          <Languages languages={languages} />
        </aside>
      </main>
    </>
  )
}
