import { Image } from '@src/Components/Image'
import { PersonalInformationType } from '@src/Types/PersonalInformationType'
import { FunctionComponent } from 'react'
import styles from './PersonalInformation.module.scss'

type PersonalInformationProps = {
  personalInformation: PersonalInformationType
}

export const PersonalInformation: FunctionComponent<PersonalInformationProps> = ({
  personalInformation: { fullName, address, email, phoneNumber },
}) => (
  <main id={styles.personalInformation}>
    <h1>{fullName}</h1>
    <section>
      <Image src="/images/address.png" alt="dirección" />
      <p>{address}</p>
    </section>
    <section>
      <Image src="/images/mail.png" alt="correo electrónico" />
      <p>{email}</p>
    </section>
    <section>
      <Image src="/images/phone.png" alt="número telefónico" />
      <p>{phoneNumber}</p>
    </section>
  </main>
)
