export type PersonalInformationType = {
  fullName: string
  address: string
  email: string
  phoneNumber: string
}
