import { Image } from '@src/Components/Image'
import { SkillsType } from '@src/Types/SkillsType'
import { FunctionComponent } from 'react'
import styles from './Skills.module.scss'

export const Skills: FunctionComponent<{ skills: SkillsType[] }> = ({ skills }) => (
  <main id={styles.skills}>
    <Image src="/images/skills.png" alt="" />
    <h1>Skills</h1>
    <div className={styles.skillList}>
      {skills.map(({ title, keywords, level }) => (
        <section key={title}>
          <h2>{title}</h2>
          <span>{keywords.join(' · ')}</span>
          <progress value={level} max="100">
            {level}%
          </progress>
        </section>
      ))}
    </div>
  </main>
)
