export type SkillsType = {
  title: string
  keywords: string[]
  level: number
}
