import { Image } from '@src/Components/Image'
import { FunctionComponent } from 'react'
import styles from './ProfessionalSummary.module.scss'

export const ProfessionalSummary: FunctionComponent<{ summary: string[] }> = ({ summary }) => {
  return (
    <main id={styles.professionalSummary}>
      <Image src="/images/summary.png" alt="" />
      <h1>Professional Summary</h1>
      <section>
        {summary.map((paragraph, index) => (
          <p key={index}>{paragraph}</p>
        ))}
      </section>
    </main>
  )
}
