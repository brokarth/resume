import { FunctionComponent } from 'react'
import { Image } from '@src/Components/Image'
import styles from './EmploymentHistory.module.scss'
import { JobType } from '@src/Types/JobType'

export const EmploymentHistory: FunctionComponent<{
  jobHistory: JobType[]
}> = ({ jobHistory }) => (
  <main id={styles.employmentHistory}>
    <Image src="/images/job.png" alt="" />
    <h1>Employment History</h1>
    <div className={styles.jobHistory}>
      {jobHistory.map(({ job, employer, from, to, location, responsibilities }) => (
        <section key={`${from}-${to}`}>
          <h1>
            {job}
            <span> @ {employer}</span>
          </h1>
          <span>{location}</span>
          <time>
            {from} - {to ?? 'current'}
          </time>
          <ul>
            {responsibilities.map((responsibilitie, index) => (
              <li key={index}>{responsibilitie}</li>
            ))}
          </ul>
        </section>
      ))}
    </div>
  </main>
)
