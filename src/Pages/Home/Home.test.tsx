import { Home } from '@src/Pages/Home'
import { render, screen, within } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

describe('Home', () => {
  const OLD_ENV = process.env

  beforeEach(() => {
    jest.resetModules()
    process.env = { ...OLD_ENV }
  })

  afterAll(() => {
    process.env = OLD_ENV
  })

  test('Should show print button', () => {
    const printMock = jest.fn()
    global.window.print = printMock
    render(<Home />)
    userEvent.click(screen.getByText(/^Imprimir$/))
    expect(printMock).toHaveBeenCalled()
  })

  test('Should show personal information', () => {
    render(<Home />)
    expect(screen.getByText(/^Nombre Paterno Materno$/)).toBeDefined()
    expect(screen.getByAltText(/^dirección$/)).toBeDefined()
    expect(screen.getByText(/^Direccion 123, Santiago, RM$/)).toBeDefined()
    expect(screen.getByAltText(/^correo electrónico$/)).toBeDefined()
    expect(screen.getByText(/^correo@test.com$/)).toBeDefined()
    expect(screen.getByAltText(/^número telefónico$/)).toBeDefined()
    expect(screen.getByText(/^\+569 1111 1111$/)).toBeDefined()
  })

  test('Should add base path to image src', () => {
    process = { ...process, env: { ...process.env, NODE_ENV: 'production' } }

    render(<Home />)
    expect(screen.getByAltText(/^Imprimir$/).getAttribute('src')).toBe('/resume/images/printer.png')
  })

  test('Should show multiline paragraphs on summary', () => {
    render(<Home />)

    // eslint-disable-next-line testing-library/no-node-access
    const { getByText } = within(document.querySelector('#professionalSummary') as HTMLElement)
    expect(getByText(/^Lorea El Ipsum Buqué oe hermano/)).toBeDefined()
    expect(getByText(/^Longi jato readi de corte saque hermano/)).toBeDefined()
  })

  test.each([
    {
      index: 1,
      job: /^DevOps \/ SecOps$/,
      employer: /^@ Company 1$/,
      location: /^Santiago, Chile$/,
      employmentTime: /^Feb. 2022 - current$/,
      responsibilities: /^Lorea El Ipsum Buqué oe hermano \d$/,
      expectedResponsibilities: 5,
    },
    {
      index: 2,
      job: /^Full stack developer$/,
      employer: /^@ Company 2$/,
      location: /^Santiago, Chile$/,
      employmentTime: /^Apr. 2020 - Feb. 2022$/,
      responsibilities: /^Longi jato readi de corte saque hermano \d$/,
      expectedResponsibilities: 3,
    },
  ])(
    'Should show $job information',
    ({ index, job, employer, location, employmentTime, responsibilities, expectedResponsibilities }) => {
      render(<Home />)

      expect(screen.getByText(/^Employment History$/)).toBeDefined()

      const { getByText, getAllByText } = within(
        // eslint-disable-next-line testing-library/no-node-access
        document.querySelector(`#employmentHistory section:nth-child(${index})`) as HTMLElement
      )
      expect(getByText(job)).toBeDefined()
      expect(getByText(employer)).toBeDefined()
      expect(getByText(location)).toBeDefined()
      expect(getByText(employmentTime)).toBeDefined()
      expect(getAllByText(responsibilities)).toHaveLength(expectedResponsibilities)
    }
  )

  test.each([
    { index: 1, title: /^JavaScript$/, keywords: /^mootools · jQuery · HTML 2.0$/, level: 80 },
    { index: 2, title: /^JAVA$/, keywords: /^ant · spring · Hibernate$/, level: 95 },
    { index: 3, title: /^CSS$/, keywords: /^Stylus · Sass · Bootstrap$/, level: 100 },
  ])('Should show skills $title', ({ index, title, keywords, level }) => {
    render(<Home />)

    expect(screen.getByText(/^Skills$/)).toBeDefined()

    const { getByText, getByRole } = within(
      // eslint-disable-next-line testing-library/no-node-access
      document.querySelector(`#skills section:nth-of-type(${index})`) as HTMLElement
    )
    expect(getByText(title)).toBeDefined()
    expect(getByText(keywords)).toBeDefined()
    expect(getByRole('progressbar')).toHaveValue(level)
  })

  test('Should show languages ', () => {
    render(<Home />)

    expect(screen.getByText(/^Languages$/)).toBeDefined()

    const { getByText, getByRole } = within(
      // eslint-disable-next-line testing-library/no-node-access
      document.querySelector(`#languages section:first-child`) as HTMLElement
    )
    expect(getByText(/^Español$/)).toBeDefined()
    expect(getByText(/^\(native\)$/)).toBeDefined()
    expect(getByRole('progressbar')).toHaveValue(100)
  })
})
